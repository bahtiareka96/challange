package id.nxy.appku

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        openActivityOne()
        openActivityTwo()


    }

    private fun openActivityOne() {
        val buttonOne = findViewById<Button>(R.id.buttonSatu)
        buttonOne.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
        }
    }

    private fun openActivityTwo() {
        val buttonTwo = findViewById<Button>(R.id.buttonDua)
        buttonTwo.setOnClickListener {
            val intent = Intent(this, MainActivity3::class.java)
            startActivity(intent)
        }
    }



}